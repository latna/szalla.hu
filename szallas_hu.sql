-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2023. Nov 29. 09:59
-- Kiszolgáló verziója: 10.4.28-MariaDB
-- PHP verzió: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `szallas.hu`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `companies`
--

CREATE TABLE `companies` (
  `companyId` bigint(20) UNSIGNED NOT NULL,
  `companyName` varchar(50) NOT NULL,
  `companyRegistrationNumber` varchar(11) NOT NULL,
  `companyFoundationDate` date NOT NULL,
  `country` varchar(50) NOT NULL,
  `zipCode` varchar(20) NOT NULL,
  `city` varchar(50) NOT NULL,
  `streetAddress` varchar(80) NOT NULL,
  `latitude` double(10,6) NOT NULL,
  `longitude` double(10,6) NOT NULL,
  `companyOwner` varchar(50) NOT NULL,
  `employees` tinyint(4) NOT NULL,
  `activity` varchar(30) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `companies`
--

INSERT INTO `companies` (`companyId`, `companyName`, `companyRegistrationNumber`, `companyFoundationDate`, `country`, `zipCode`, `city`, `streetAddress`, `latitude`, `longitude`, `companyOwner`, `employees`, `activity`, `active`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Mauris PC', '177874-5578', '1990-05-01', 'Belarus', '71137', 'Villa Verde', 'P.O. Box 391, 1457 Sed St.', 50.670630, -75.377000, 'Travis Elliott', 11, 'Car', 0, 'Donec.sollicitudin@Duisacarcu.com', 'felis', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(2, 'Magna Lorem Inc.', '896098-0855', '1991-10-22', 'Philippines', '60934', 'Bahawalnagar', '6753 Sit Street', 4.696710, -101.066120, 'Hamilton Pearson', 62, 'Building Industry', 0, 'metus.facilisis@Proinsedturpis.ca', 'sem,', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(3, 'Egestas Hendrerit Neque LLP', '615308-4733', '1994-06-10', 'Western Sahara', '442580', 'Lagundo/Algund', 'P.O. Box 857, 1455 Nullam Street', 89.758930, -125.999410, 'Todd Douglas', 127, 'Building Industry', 1, 'lacus.Quisque@posuerecubiliaCurae.org', 'vestibulum', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(4, 'Ipsum Ac Mi Company', '609476-5515', '1984-11-28', 'Northern Mariana Islands', '64967', 'Lonquimay', 'Ap #191-7809 Nec Ave', 71.412730, -64.463020, 'Hayes Harper', 127, 'Growing Plants', 0, 'convallis.dolor@perinceptos.ca', 'nulla.', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(5, 'Nibh Phasellus Nulla Consulting', '875175-2307', '2013-12-12', 'French Guiana', '61307', 'Pelluhue', 'P.O. Box 371, 6261 Non, St.', -17.639400, 55.737540, 'Norman Walls', 127, 'IT', 1, 'massa.non.ante@auctor.co.uk', 'nec', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(6, 'Ac Corp.', '047597-1263', '1990-05-12', 'United Arab Emirates', '6648', 'Flawinne', '481-9974 Vestibulum Road', 81.445000, -103.015620, 'Jerry Herman', 127, 'Car', 1, 'ornare.sagittis@orciadipiscing.org', 'ornare,', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(7, 'Ut Molestie Company', '387343-2466', '2019-11-08', 'Algeria', 'DB15 0TF', 'Lacombe County', 'Ap #781-3660 Leo. Av.', -11.060260, -150.114690, 'Nicholas Stuart', 127, 'Building Industry', 1, 'Donec@Etiam.com', 'nonummy', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(8, 'Justo Eu Arcu Inc.', '495058-7198', '2015-07-02', 'Denmark', '2116', 'Selkirk', 'Ap #385-8575 Nullam Rd.', 9.440150, -126.009970, 'Amery Huber', 127, 'Food', 0, 'mollis.dui.in@utpharetrased.co.uk', 'vulputate', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(9, 'Eu Institute', '496281-9175', '2000-06-11', 'Albania', '9469', 'Pittsburgh', 'P.O. Box 924, 1815 Elit. Av.', -83.857540, 21.331760, 'Gage Duncan', 99, 'Food', 1, 'Nunc@ac.org', 'Sed', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(10, 'Rutrum Consulting', '404429-0841', '1995-12-02', 'Korea, North', '528938', 'Kamarhati', 'Ap #732-2307 Augue St.', 86.505860, -94.034200, 'Hashim Curry', 127, 'Growing Plants', 0, 'quis.turpis@conubianostra.org', 'congue.', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(11, 'Dapibus Quam Limited', '874092-9669', '2014-09-14', 'Nauru', 'Z5 0NG', 'Laon', '150-2314 Dis Rd.', -61.578300, -113.533500, 'Paki Elliott', 127, 'Building Industry', 0, 'non.nisi@et.org', 'dolor', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(12, 'Ac Institute', '147153-5631', '2020-06-25', 'Ecuador', '8918', 'Westmount', '757-1405 Tempor, Rd.', -29.215390, 80.319990, 'Nathaniel Summers', 127, 'Growing Plants', 1, 'ullamcorper.magna@massaQuisque.ca', 'In', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(13, 'Adipiscing Fringilla Consulting', '943211-1384', '2008-07-01', 'Armenia', 'V9N 3K8', 'La Cisterna', '389-4506 Malesuada Rd.', -40.909900, 0.555580, 'Gray Nicholson', 127, 'Growing Plants', 0, 'amet.massa@Curabitur.co.uk', 'lorem', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(14, 'Urna Et Arcu Incorporated', '296422-0780', '1980-03-13', 'Falkland Islands', '51-352', 'Genk', 'Ap #126-6407 Ullamcorper, St.', -48.304110, 166.025670, 'Joel Bartlett', 48, 'Growing Plants', 1, 'egestas.nunc@lobortis.net', 'enim', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(15, 'Nam Porttitor Scelerisque Industries', '832132-1518', '1982-04-13', 'Ireland', '926236', 'Remscheid', 'P.O. Box 648, 9240 Suspendisse St.', 34.055350, 71.442960, 'Cain Coleman', 117, 'Car', 1, 'quam.Curabitur.vel@Vivamusnibhdolor.co.uk', 'Pellentesque', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(16, 'Mauris PC', '038700-0656', '1989-02-16', 'Dominica', '57717', 'Norrköping', 'Ap #859-4029 Vivamus Av.', -79.151520, -16.952430, 'Joshua Farley', 127, 'Building Industry', 1, 'lacinia.Sed@mauriseu.ca', 'faucibus', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(17, 'Rhoncus Nullam Velit LLP', '113075-2072', '1977-01-29', 'Lesotho', '98940', 'Ipís', '3927 Adipiscing Av.', -5.635820, 156.311940, 'Grady Brady', 127, 'Car', 1, 'commodo@conguea.com', 'adipiscing', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(18, 'Sagittis Placerat PC', '897659-4542', '2000-07-27', 'Marshall Islands', '5625', 'Dokkum', '1405 Mauris St.', -39.514260, 58.908490, 'Hoyt Sparks', 127, 'Car', 1, 'in.consequat@tellusidnunc.net', 'amet', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(19, 'Ligula Aliquam Erat PC', '349714-9892', '1998-04-19', 'Sri Lanka', '18789-978', 'Kalat', '9174 Morbi Street', 66.415820, 41.437610, 'Jamal Miller', 127, 'IT', 0, 'Cras.dolor@egestashendrerit.edu', 'magna.', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(20, 'Enim Consequat Corporation', '468296-7726', '1993-11-06', 'Bosnia and Herzegovina', '25017', 'Kassel', '207-8810 Eleifend Street', 18.592960, 169.043220, 'Blake Armstrong', 127, 'Car', 0, 'facilisi.Sed.neque@Cras.ca', 'sodales', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(21, 'Vulputate Mauris Limited', '015506-0312', '1978-03-28', 'Czech Republic', '92256', 'Bath', 'P.O. Box 593, 1211 Enim Rd.', -69.008510, -146.536610, 'Griffin Mcgowan', 127, 'Building Industry', 0, 'In@erat.org', 'justo.', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(22, 'Aliquam Enim Corporation', '298024-3410', '1997-04-15', 'Finland', '40134-63707', 'Arbre', 'P.O. Box 812, 6627 Ridiculus Rd.', 33.131490, 113.640740, 'Ralph Everett', 94, 'Building Industry', 1, 'Nulla@odio.co.uk', 'risus', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(23, 'Ac Consulting', '213715-0500', '1994-05-08', 'Norway', '27343', 'Maransart', 'Ap #733-9569 Tempor Street', 53.311150, -122.270910, 'Hiram Jimenez', 127, 'Growing Plants', 1, 'tortor.dictum.eu@Integer.co.uk', 'eleifend.', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(24, 'Aliquet Phasellus Foundation', '299975-9497', '2016-07-19', 'Saint Lucia', '94634', 'Denderbelle', 'P.O. Box 350, 5648 Rutrum. Av.', -19.411370, 63.362400, 'Ezra Garrison', 127, 'IT', 1, 'aliquet.molestie@dolor.edu', 'lectus', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(25, 'Vel Lectus Consulting', '284240-1461', '1980-07-23', 'Bermuda', '69530', 'Ingelheim', '868-3049 Aliquam Ave', -69.106170, -39.259670, 'Cain Colon', 127, 'Growing Plants', 1, 'vulputate@odioNaminterdum.org', 'Aliquam', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(26, 'Diam Lorem Auctor Limited', '737022-2007', '2014-08-06', 'Czech Republic', '3025', 'Retiro', '115-4328 Mollis. Avenue', 25.261540, 88.272360, 'Lucian Guthrie', 55, 'Car', 1, 'iaculis.odio@risusodioauctor.org', 'felis.', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(27, 'Fusce Aliquet LLC', '505774-5464', '2012-07-07', 'French Guiana', '56191', 'Sambreville', 'P.O. Box 611, 2079 A Street', -45.028080, -32.710530, 'Gray Elliott', 127, 'Building Industry', 1, 'Duis.a.mi@faucibus.org', 'accumsan', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(28, 'Duis Risus Odio Associates', '664171-0568', '1981-07-29', 'Martinique', '42584', 'Salamanca', '621-155 Non Rd.', -56.148180, -23.177690, 'Justin Deleon', 127, 'Car', 0, 'rhoncus.Proin@nullaatsem.org', 'id', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(29, 'Mattis Ornare Incorporated', '644302-8698', '1976-06-18', 'Macao', '492928', 'Leipzig', 'Ap #418-6201 Aliquam Av.', -39.225810, 42.645470, 'Kareem Freeman', 127, 'Growing Plants', 0, 'commodo@mauris.com', 'lacus.', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(30, 'Nisl Corp.', '446969-7454', '1979-09-25', 'China', '18662', 'Znamensk', '527-6157 Feugiat St.', -71.068290, -7.766100, 'Philip Garcia', 127, 'Car', 0, 'faucibus@nibhQuisque.net', 'enim.', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(31, 'Ligula Aenean Gravida Consulting', '165190-5026', '1977-02-06', 'Suriname', '51530', 'Brighton', '528-2561 Morbi Ave', 20.876190, 37.103340, 'Rooney Gardner', 101, 'Building Industry', 1, 'risus.Duis.a@malesuadafames.ca', 'urna.', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(32, 'Cras Sed Institute', '793259-0073', '2018-10-28', 'Tonga', 'Z8134', 'Sitapur', '976-2786 Neque Ave', -78.819510, 102.883790, 'Lucius Miller', 127, 'Growing Plants', 0, 'porttitor.scelerisque.neque@lectusCum.edu', 'Nunc', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(33, 'Nibh Ltd', '609207-8762', '2007-11-21', 'Kiribati', 'T49 8AU', 'Chimbarongo', '829-2022 Dis Ave', -46.724760, 147.204190, 'Gannon Park', 127, 'Building Industry', 0, 'in.faucibus@vulputate.net', 'ipsum.', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(34, 'Curae; Phasellus Limited', '308488-6708', '2009-05-22', 'Kyrgyzstan', 'MX2C 2DE', 'Ebenthal in Kärnten', '703-8299 Tellus. St.', 55.560070, 57.542540, 'Levi Vance', 25, 'Building Industry', 0, 'cursus.et.eros@urnaNuncquis.org', 'amet', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(35, 'Nec Diam PC', '384120-5713', '1989-06-14', 'Jersey', 'Z5716', 'Ciudad Obregón', 'P.O. Box 671, 5515 Ornare St.', 50.927430, -127.582840, 'Neil Hampton', 62, 'Car', 1, 'magna.tellus@Quisquetinciduntpede.edu', 'Integer', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(36, 'Sem Egestas Blandit Foundation', '298751-6321', '1976-09-20', 'Congo, the Democratic Republic of the', 'L0Z 4Y2', 'Alcalá de Henares', 'Ap #589-6402 In Av.', -35.612600, -132.463250, 'Steel Watkins', 127, 'Growing Plants', 0, 'dictum@necleo.net', 'urna', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(37, 'Enim Foundation', '479882-8747', '2017-02-11', 'Central African Republic', 'E4T 4W7', 'Tonk', 'P.O. Box 250, 6935 Lorem, St.', 71.151530, -22.736780, 'Oleg Walter', 127, 'Growing Plants', 0, 'montes.nascetur@mattisvelit.com', 'eget,', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(38, 'Orci Company', '193154-2755', '2004-12-25', 'Singapore', 'RE42 1LB', 'Madrid', '944-3039 Consectetuer Rd.', -17.899470, 177.505420, 'Hunter Walls', 127, 'Food', 1, 'Suspendisse.aliquet.molestie@utcursus.com', 'at', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(39, 'Nisi Magna Industries', '458912-8406', '1995-05-12', 'Lithuania', 'C88 1SA', 'Bonnyrigg', '540-4734 Nisi St.', -40.420550, 126.406500, 'Baker Stephenson', 127, 'Food', 1, 'mattis.semper.dui@velit.co.uk', 'urna', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(40, 'In At Pede Ltd', '399639-6861', '2007-05-19', 'Norway', '46738', 'Gorbea', 'Ap #494-1350 Orci, Rd.', -44.487840, 21.207290, 'Bradley Small', 127, 'Building Industry', 1, 'Nam.porttitor@lacusUt.com', 'sodales', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(41, 'Bibendum Ullamcorper Duis LLP', '697656-2709', '2012-05-07', 'Armenia', '4421', 'Zona Bananera', 'P.O. Box 798, 7239 Ligula. St.', -26.719340, 136.003690, 'Aidan Ortiz', 127, 'Growing Plants', 0, 'Nunc.quis.arcu@anunc.org', 'arcu.', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(42, 'Orci Phasellus LLP', '862779-8948', '2019-02-06', 'Guinea-Bissau', '60410', 'Springfield', 'Ap #759-3004 Eu, Ave', 29.011830, -112.709220, 'Camden Neal', 127, 'Building Industry', 1, 'eu@sed.ca', 'lacus.', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(43, 'Etiam Laoreet Libero PC', '505947-1119', '1980-02-29', 'Isle of Man', '2940', 'Fort McPherson', 'Ap #205-6670 Lobortis Street', 64.295020, -9.126110, 'Hilel Hays', 98, 'Growing Plants', 1, 'mauris@Crasvehiculaaliquet.edu', 'pharetra', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(44, 'Congue Elit Sed PC', '054324-8066', '1981-12-22', 'Curaçao', '37939', 'Belo Horizonte', '6105 Non Av.', 14.908960, 43.657680, 'Walter Riggs', 127, 'Growing Plants', 0, 'imperdiet.nec@velsapien.ca', 'in', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(45, 'Tincidunt Congue Turpis Corporation', '665763-8158', '2017-12-18', 'Hong Kong', '291325', 'Aieta', 'P.O. Box 673, 7687 Dui St.', 36.763400, 17.966230, 'Paul Lancaster', 112, 'Growing Plants', 1, 'natoque.penatibus@nisl.ca', 'quis', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(46, 'Netus Et Malesuada Corporation', '348579-3487', '2001-12-06', 'Burkina Faso', '1174', 'Wagga Wagga', '319-2330 Lacinia St.', 34.725680, 167.763030, 'Wayne Tucker', 127, 'Car', 1, 'eu.tellus.Phasellus@Quisquefringilla.net', 'orci', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(47, 'Vehicula Pellentesque Corp.', '809126-6471', '1980-10-06', 'Dominica', '1028', 'Mainz', '788-8720 Velit Street', -86.075040, 70.402590, 'Samuel Le', 127, 'Building Industry', 0, 'Donec@tellusAeneanegestas.edu', 'lacus.', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(48, 'Ante Bibendum Ullamcorper Foundation', '952209-0514', '1987-04-09', 'Jersey', '984637', 'Bouwel', 'P.O. Box 863, 9246 Interdum. Avenue', 76.396940, 34.351330, 'Aristotle Noble', 127, 'Growing Plants', 1, 'Fusce.dolor@mauriselitdictum.edu', 'aliquam', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(49, 'Mauris Ipsum Inc.', '229141-4403', '1999-09-02', 'Laos', 'Z1900', 'Villers-sur-Semois', '7076 Et Av.', -55.295310, -85.938360, 'Phillip Erickson', 3, 'Growing Plants', 0, 'augue.malesuada.malesuada@sodalesMauris.co.uk', 'magna.', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(50, 'Sit Amet Consectetuer Inc.', '229482-3634', '1977-12-19', 'Moldova', '5738', 'Ereğli', 'Ap #320-4007 Mi Avenue', -64.518290, -165.125480, 'Uriel Blair', 99, 'Car', 0, 'nibh@diamluctus.edu', 'eget,', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(51, 'Accumsan Laoreet Ipsum LLP', '685555-2490', '2000-07-31', 'Mauritania', '19417', 'Galzignano Terme', '617-2164 Sem Rd.', 16.836730, 29.976780, 'Alec Best', 127, 'Food', 0, 'ut@loremvehicula.net', 'nec', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(52, 'Ut Erat Sed LLC', '616405-8320', '1981-12-01', 'Barbados', '5855', 'General Escobedo', '7817 Mauris Road', -10.944250, 78.598530, 'Brett Vance', 43, 'Building Industry', 1, 'tellus.faucibus.leo@Donecnibh.com', 'cursus', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(53, 'Diam Proin Limited', '043851-7658', '1997-12-28', 'France', 'WO9J 9RO', 'Reno', 'P.O. Box 229, 7505 Orci St.', 87.773860, 168.016040, 'Vernon Harris', 56, 'Building Industry', 1, 'sem.Nulla.interdum@luctusfelis.org', 'Etiam', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(54, 'Non Sapien Industries', '592842-3614', '1993-11-14', 'Reunion', '8630 PX', 'Blankenberge', 'P.O. Box 919, 7986 Consequat Ave', 76.448350, 99.700480, 'Cole Hopper', 72, 'IT', 1, 'metus.Aliquam.erat@ametrisusDonec.co.uk', 'luctus,', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(55, 'Feugiat Metus Sit Ltd', '296701-2515', '1976-10-31', 'Sweden', '40323', 'Llanquihue', 'Ap #486-6806 Ornare Rd.', 11.363570, 8.238150, 'Colton Stein', 127, 'Car', 0, 'Donec@acsemut.co.uk', 'parturient', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(56, 'Et Euismod Et Consulting', '934575-2878', '2019-02-21', 'Curaçao', '55507', 'Warminster', 'Ap #974-3390 Suspendisse Avenue', -58.959480, 29.229720, 'Demetrius Holloway', 127, 'Food', 0, 'Proin.nisl.sem@mattis.ca', 'primis', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(57, 'Mollis PC', '212117-5679', '1989-02-17', 'Albania', '2938', 'Jackson', 'Ap #348-2018 Lectus Rd.', -51.363970, -149.814560, 'Eaton Pennington', 127, 'Building Industry', 1, 'vitae.erat@sapien.org', 'egestas,', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(58, 'Aliquet Molestie Tellus Company', '857403-6045', '1998-11-20', 'Swaziland', '8046', 'Eisenhüttenstadt', 'P.O. Box 303, 3224 Vitae Rd.', 17.362830, -10.461600, 'Emmanuel Bishop', 127, 'Growing Plants', 1, 'Etiam@ullamcorperviverraMaecenas.com', 'nunc.', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(59, 'Odio Semper Cursus Limited', '598188-9735', '2014-11-13', 'Spain', '83062', 'Stroitel', '974-1180 Cum Road', 59.951130, 51.249920, 'Timon Hatfield', 127, 'Car', 0, 'ipsum@nisiMauris.edu', 'sem', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(60, 'Sit PC', '496815-6002', '2002-03-12', 'Heard Island and Mcdonald Islands', '1452', 'Villers-Poterie', 'P.O. Box 880, 9209 Volutpat St.', 78.586190, -139.044530, 'Timothy Levy', 58, 'Building Industry', 0, 'Donec.tincidunt.Donec@Nullamut.ca', 'Suspendisse', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(61, 'Urna Suscipit Nonummy Consulting', '436989-3856', '2010-05-08', 'Kuwait', '54468', 'Falciano del Massico', '527-4117 Nisi St.', 85.661140, 128.918690, 'Josiah Dunn', 127, 'Food', 0, 'Quisque@semmollis.com', 'pharetra,', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(62, 'Posuere Cubilia Curae; Corp.', '987019-0098', '2009-05-01', 'Cuba', '71-267', 'Plymouth', '6625 Orci. Av.', -76.552810, 157.257540, 'Joshua Roth', 127, 'Food', 1, 'Sed.nulla.ante@elitdictum.org', 'risus.', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(63, 'Proin Non Massa Industries', '844094-8498', '1981-09-07', 'Grenada', '40306', 'Bhind', '311-244 Nullam Ave', -33.488500, -76.954410, 'Aquila Campbell', 127, 'Car', 0, 'quis@quisarcuvel.com', 'fames', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(64, 'Nam Porttitor Scelerisque Incorporated', '529736-5081', '1985-07-31', 'Mali', '2516', 'Tiltil', '2838 Non St.', -83.879900, -160.584470, 'Micah Kim', 82, 'Growing Plants', 0, 'dignissim.magna@Maurisnondui.ca', 'lorem,', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(65, 'Dui Semper Associates', '686201-0441', '1986-03-20', 'Turkmenistan', '60112', 'Zandvoorde', '908-9132 Mi St.', 18.870490, -5.200580, 'Chandler Vasquez', 127, 'Building Industry', 0, 'magnis.dis.parturient@enim.net', 'vulputate', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(66, 'Lectus Company', '148952-0740', '2004-03-05', 'Botswana', '93877', 'Bonnyrigg', 'Ap #851-5106 Risus. Avenue', -72.441280, -167.341570, 'Leo Hartman', 127, 'IT', 1, 'vitae@duiquisaccumsan.edu', 'tortor', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(67, 'Libero Institute', '034397-9407', '1976-01-15', 'Virgin Islands, United States', '42765', 'Morinville', '413-8544 Enim. Road', -82.539250, 75.973310, 'Berk Jacobs', 47, 'IT', 0, 'Nulla.tempor.augue@litoratorquent.net', 'non', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(68, 'Ac Nulla Corp.', '086707-9717', '1997-02-26', 'Kyrgyzstan', '51711', 'Colledimacine', '3553 Elementum Avenue', 69.521460, -66.089750, 'Zeus Crawford', 127, 'IT', 0, 'erat.vitae.risus@Craseutellus.edu', 'Morbi', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(69, 'Sit Amet Metus PC', '636578-3247', '1975-08-06', 'Turkey', 'RA5 1HG', 'Taber', 'P.O. Box 885, 6937 Aliquet Road', -75.480270, -107.608120, 'Dieter Vaughn', 12, 'Food', 1, 'purus.in@pellentesque.org', 'at,', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(70, 'Nulla Facilisi LLP', '548134-2904', '1999-02-19', 'Northern Mariana Islands', '67111', 'Requínoa', '1667 Bibendum Ave', 48.604930, -174.399380, 'Jackson Baird', 127, 'Growing Plants', 1, 'dui@aliquam.edu', 'arcu.', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(71, 'Integer Sem Consulting', '172914-2701', '2016-03-10', 'Libya', '17008', 'Llanquihue', '815-4103 Ac, Street', -37.443740, -171.153750, 'Harlan Ferrell', 127, 'Car', 1, 'eros.nec.tellus@Etiamgravida.net', 'torquent', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(72, 'Fermentum Arcu Vestibulum PC', '296427-5461', '2016-04-06', 'Tajikistan', '413156', 'Rostock', 'P.O. Box 667, 6377 Egestas Av.', 53.418350, -4.234150, 'Adam Cole', 19, 'Car', 1, 'elit.Nulla@nisiAenean.org', 'penatibus', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(73, 'Justo Foundation', '357840-0222', '1984-11-02', 'Niger', '353691', 'Puerto Colombia', '4289 Eu Rd.', -9.453010, 60.359980, 'Gavin Powell', 127, 'IT', 0, 'quam.a.felis@bibendum.net', 'facilisis.', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(74, 'Hendrerit A Arcu Inc.', '148044-0179', '2006-09-21', 'Austria', '16413', 'Freirina', '291-5391 Mi Road', -35.069630, -103.104510, 'Sebastian Mccall', 127, 'Building Industry', 0, 'diam.Sed.diam@dictum.edu', 'augue', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(75, 'Cursus Consulting', '976892-8765', '1994-09-08', 'Mexico', 'Z6686', 'Camponogara', 'Ap #249-9275 Mi Road', 55.199390, 38.594270, 'Jonas Burt', 127, 'Building Industry', 1, 'adipiscing.elit@aptenttacitisociosqu.ca', 'pede.', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(76, 'Eu Company', '319945-3329', '1994-09-25', 'Nigeria', '51845', 'Siquirres', 'P.O. Box 474, 4246 Phasellus Avenue', 1.857690, 121.761720, 'Thomas Manning', 127, 'IT', 0, 'Nullam.enim@nec.net', 'erat', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(77, 'Fusce Feugiat Lorem Industries', '343080-5907', '1999-05-12', 'Uruguay', '51417', 'Surabaya', '854-2043 Facilisis St.', 35.161040, -59.117060, 'Cullen Lynch', 28, 'Building Industry', 0, 'tempor.lorem@Etiambibendumfermentum.org', 'ullamcorper', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(78, 'Tortor PC', '125087-8269', '2016-11-04', 'Equatorial Guinea', '73642', 'Meeuwen-Gruitrode', 'P.O. Box 188, 2095 Turpis Avenue', 81.666850, -132.952890, 'Connor Barrera', 127, 'Car', 0, 'leo.elementum.sem@metus.ca', 'felis,', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(79, 'Et Ultrices Posuere Company', '659366-9846', '2002-04-08', 'Estonia', '12365', 'Sudhanoti', '6336 Risus. Rd.', 84.487590, 24.764250, 'Ahmed Calhoun', 90, 'Building Industry', 1, 'risus@sociis.net', 'faucibus', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(80, 'Euismod Ltd', '505754-2507', '1981-03-26', 'Papua New Guinea', '85892', 'Joondalup', 'Ap #285-6662 Non Road', -48.428630, -112.470540, 'Emmanuel Perez', 127, 'Growing Plants', 0, 'Cum.sociis@Duis.co.uk', 'felis,', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(81, 'Donec Company', '905581-3969', '1998-01-23', 'Lebanon', 'Z8653', 'Bihain', 'P.O. Box 952, 5590 Accumsan Rd.', 23.313530, -148.370500, 'Lev Simon', 127, 'Building Industry', 0, 'Proin@molestiein.edu', 'lacinia', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(82, 'Neque Nullam Corporation', '496430-6353', '1994-04-16', 'Bangladesh', '27618-39013', 'Montague', 'P.O. Box 764, 1587 Dis Rd.', 31.908240, 150.075720, 'Nathaniel Burt', 127, 'Car', 1, 'elit.fermentum@Phasellus.co.uk', 'libero', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(83, 'Vulputate Associates', '673363-7430', '1996-06-06', 'Ireland', '37724', 'Bras', 'P.O. Box 922, 8151 Volutpat Rd.', 74.147390, -59.659050, 'Armand Howell', 127, 'Growing Plants', 1, 'dis.parturient@eros.co.uk', 'lacus.', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(84, 'Ligula Aliquam Erat Ltd', '806640-1772', '2016-11-07', 'Singapore', '697586', 'Dole', 'P.O. Box 722, 8313 Laoreet Road', -34.460510, 13.727110, 'Patrick Mcfarland', 127, 'IT', 0, 'orci@pede.com', 'interdum', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(85, 'Tortor Integer Associates', '359548-8556', '1976-12-10', 'Ethiopia', '85437', 'Salamanca', 'P.O. Box 987, 7081 Sem. Avenue', -82.422140, 116.385780, 'Isaiah Love', 127, 'Building Industry', 0, 'feugiat.nec.diam@sagittis.org', 'orci', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(86, 'Ligula LLC', '956856-4844', '1996-07-24', 'Cuba', '7748', 'Río Ibáñez', 'P.O. Box 335, 394 Dictum Av.', -23.006520, 96.819380, 'Griffin Mcintyre', 127, 'Car', 0, 'augue.scelerisque.mollis@afelisullamcorper.net', 'Mauris', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(87, 'Et Commodo Limited', '442801-8735', '2002-12-20', 'France', '8396', 'Rixensart', '7720 Quisque Ave', 60.009470, 66.993720, 'Kermit Maddox', 127, 'Growing Plants', 1, 'penatibus@dapibusidblandit.edu', 'enim', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(88, 'Aliquet Metus Urna Associates', '203888-3068', '2004-12-05', 'Tonga', '241312', 'Allentown', 'Ap #632-6950 In Street', 53.254030, 132.633570, 'Fletcher Langley', 127, 'Car', 0, 'dictum@egestasSed.ca', 'mauris.', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(89, 'Elit Sed Consequat Foundation', '742337-5281', '1976-01-08', 'Guinea-Bissau', '72654-179', 'Cinisi', '2153 Sit Avenue', -73.941140, 142.514480, 'Austin Mayer', 57, 'Car', 0, 'a.arcu@eget.com', 'vitae', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(90, 'Elit Pede LLP', '484250-8378', '2017-11-24', 'Comoros', '870133', 'Le Cannet', 'Ap #112-122 In Street', -43.281770, 148.362250, 'Mark Alvarez', 127, 'Building Industry', 1, 'Donec.est@Sed.edu', 'id,', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(91, 'Elementum LLP', '503420-9477', '2008-12-11', 'Saint Pierre and Miquelon', '2589', 'Reno', 'P.O. Box 805, 3745 Ultrices Av.', 62.520450, 120.791720, 'Francis Pacheco', 127, 'Food', 1, 'nulla@tempuseu.co.uk', 'nec', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(92, 'Et Eros Ltd', '032113-0866', '2016-07-26', 'Saudi Arabia', '99198', 'Valéncia', '811-8295 Lectus St.', 16.607930, -101.780480, 'Barclay Moran', 98, 'Building Industry', 0, 'mauris@eget.ca', 'sem', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(93, 'Sit Amet Risus Corporation', '489799-8763', '1979-05-06', 'Montenegro', '66173', 'Wambeek', 'P.O. Box 274, 579 Sit Avenue', 29.517800, 4.562060, 'Tobias Peck', 63, 'Growing Plants', 0, 'dignissim@malesuadavelconvallis.co.uk', 'facilisis', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(94, 'Tristique Company', '404399-6885', '2002-06-28', 'South Sudan', '709185', 'Ponta Grossa', 'P.O. Box 660, 911 Curabitur St.', -12.466390, -47.808950, 'Stone Fletcher', 117, 'Growing Plants', 0, 'Aliquam@odiosagittissemper.ca', 'tortor,', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(95, 'Eu Tellus Eu Associates', '148798-5010', '2001-07-18', 'Venezuela', '49633', 'Temuco', '749-2666 Volutpat. St.', -56.473090, -85.772690, 'Porter Anderson', 127, 'Car', 1, 'In.scelerisque.scelerisque@urna.com', 'eu,', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(96, 'Cursus Non Corporation', '993494-9752', '1989-09-01', 'Cayman Islands', '6489 OA', 'Lac-Serent', '7401 Blandit Av.', -59.457040, -113.119170, 'Kareem Hansen', 89, 'Car', 1, 'vitae.odio.sagittis@ridiculus.ca', 'dignissim', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(97, 'Erat LLP', '674411-4775', '2013-11-01', 'French Guiana', 'Z5708', 'Longvilly', 'Ap #960-133 Arcu. Rd.', -37.524150, -121.761640, 'Amos Osborn', 127, 'Growing Plants', 1, 'nunc@ametorci.net', 'arcu.', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(98, 'Et Consulting', '232008-1199', '2013-03-24', 'Chad', 'Z3020', 'Bowling Green', '1073 Turpis. St.', -10.238810, 16.917730, 'Palmer Norman', 127, 'Food', 1, 'libero@vitaesemper.co.uk', 'enim.', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(99, 'Lacus Associates', '219173-9727', '1989-02-08', 'Congo (Brazzaville)', '14914', 'Böblingen', '6688 Metus. St.', 76.544040, -91.161780, 'Akeem Porter', 42, 'Food', 1, 'Duis.ac.arcu@non.co.uk', 'massa.', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(100, 'Enim Associates', '488428-9150', '2011-11-11', 'Sao Tome and Principe', '44419', 'Tamworth', '5003 Quam Avenue', 72.315920, -174.924510, 'Callum Leonard', 127, 'IT', 0, 'nisi.Cum@quama.com', 'ut,', '2023-11-27 14:35:55', '2023-11-27 14:35:55'),
(101, 'Teszt Cég', '123456-1234', '2023-11-27', 'Magyarország', '1108', 'Budapest', 'Gőzmozdony utca 10 7/27', 47.469780, 19.159127, 'Laták Nándor', 15, 'Development', 1, 'lataknandi@gmail.com', 'alma1234', '2023-11-29 07:22:48', '2023-11-29 07:22:48');

--
-- Eseményindítók `companies`
--
DELIMITER $$
CREATE TRIGGER `prevent_foundation_date_update` BEFORE UPDATE ON `companies` FOR EACH ROW BEGIN
IF NEW.companyFoundationDate <> OLD.companyFoundationDate THEN
SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'The company Foundation Date field cannot be changed!';
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_11_27_084326_create_companies_table', 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`companyId`),
  ADD UNIQUE KEY `companies_email_unique` (`email`);

--
-- A tábla indexei `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- A tábla indexei `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- A tábla indexei `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- A tábla indexei `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `companies`
--
ALTER TABLE `companies`
  MODIFY `companyId` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT a táblához `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT a táblához `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
