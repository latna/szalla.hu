<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $primaryKey = 'companyId';

    protected $fillable = [
        'companyName',
        'companyRegistrationNumber',
        'companyFoundationDate',
        'country',
        'zipCode',
        'city',
        'streetAddress',
        'latitude',
        'longitude',
        'companyOwner',
        'employees',
        'activity',
        'active',
        'email',
        'password',
    ];

    protected $casts = [
        'latitude' => 'float',
        'longitude' => 'float',
        'employees' => 'integer',
        'active' => 'boolean',
    ];

    public static function validationRules($companyId = 0)
    {
        $companyFoundationDate = [];
        if ($companyId == 0) {//create
            $companyFoundationDate = ['companyFoundationDate' => 'required|date_format:Y-m-d'];
        }
        return array_merge($companyFoundationDate, [
            'companyName' => 'required|between:3,50',
            'companyRegistrationNumber' => 'required|size:11',
            'country' => 'required|between:3,50',
            'zipCode' => 'required|between:3,20',
            'city' => 'required|between:3,50',
            'streetAddress' => 'required|between:3,80',
            'latitude' => 'required|numeric|between:-90.0,90.0',
            'longitude' => 'required|numeric|between:-180.0,180.0',
            'companyOwner' => 'required|between:3,50',
            'employees' => 'required|integer|gte:1',
            'activity' => 'required|between:3,30',
            'active' => 'required|boolean',
            'email' => sprintf('required|email|unique:companies,email,%d,companyId|between:3,50', $companyId),
            'password' => 'required|between:3,50',
        ]);
    }
}
