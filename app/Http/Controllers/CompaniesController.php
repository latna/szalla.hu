<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class CompaniesController extends Controller
{

    public function listAll(): JsonResponse
    {
        $companies = Company::all();
        return response()->json(['companies' => $companies]);
    }

    public function listByIds(Request $request): JsonResponse
    {
        $ids = $request->input('ids', []);
        $companies = Company::whereIn('companyId', $ids)->get();
        return response()->json(['companies' => $companies]);
    }

    public function create(Request $request): JsonResponse
    {
        try {
            $validatedData = $request->validate(Company::validationRules());
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        }
        $company = Company::create($validatedData);
        return response()->json(['success' => $company], 201);
    }

    public function update(Request $request, string $id): JsonResponse
    {
        $company = Company::find($id);
        if (is_null($company)) {
            return response()->json(['errors' => 'not found'], 404);
        }
        try {
            $validatedData = $request->validate(Company::validationRules($company?->companyId));
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        }
        $company->update($validatedData);
        return response()->json(['success' => $company]);
    }
}
