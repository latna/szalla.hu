<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompaniesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/companies/list', [CompaniesController::class, 'listAll']);
Route::post('/companies/list-by-ids', [CompaniesController::class, 'listByIds']);
Route::post('/companies/create', [CompaniesController::class, 'create']);
Route::post('/companies/{id}/update', [CompaniesController::class, 'update']);
