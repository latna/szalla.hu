<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id('companyId');
            $table->string('companyName', 50);
            $table->string('companyRegistrationNumber', 11);
            $table->date('companyFoundationDate');
            $table->string('country', 50);
            $table->string('zipCode', 20);
            $table->string('city', 50);
            $table->string('streetAddress', 80);
            $table->double('latitude', 10, 6);
            $table->double('longitude', 10, 6);
            $table->string('companyOwner', 50);
            $table->tinyInteger('employees');
            $table->string('activity', 30);
            $table->boolean('active')->default(false);
            $table->string('email', 50)->unique();
            $table->string('password', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('companies');
    }
};
